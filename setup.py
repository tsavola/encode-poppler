#!/usr/bin/env python

# Options

import sys
import getopt

options = {
	'help': False,
	'prefix': None,
	'destdir': None,
	'no-compile': False,
}

def usage(file):
	print >>file, 'Usage:', sys.argv[0], 'install [--help] [--prefix=PATH] [--destdir=PATH] [--no-compile]'

try:
	optspec = []

	for name in options:
		if type(options[name]) is bool:
			opt = name
		else:
			opt = '%s=' % name
		optspec.append(opt)

	args = sys.argv[1:]
	install = False

	for arg in args:
		if arg == 'install':
			args.remove(arg)
			install = True
			break

	if not install:
		usage(sys.stderr)
		sys.exit(1)

	opts, args = getopt.getopt(args, '', optspec)

	if args:
		usage(sys.stderr)
		sys.exit(1)

	for opt, value in opts:
		name = opt.strip('-')
		if name in options:
			if type(options[name]) is bool:
				options[name] = True
			else:
				options[name] = value
		else:
			usage(sys.stderr)
			sys.exit(1)

	if options['help']:
		usage(sys.stdout)
		sys.exit(0)

except getopt.GetoptError:
	usage(sys.stderr)
	sys.exit(1)

# Utilities

import os
from os.path import join, split, dirname, exists, abspath
from glob import glob
import shutil
import compileall

def get_option(name, default=None):
	value = options[name]
	if value is not None:
		return value
	else:
		return default

def install(target, source):
	print 'Installing', target

	make_leading_dirs(target)
	shutil.copyfile(source, target)
	os.chmod(target, 0644)

def make_leading_dirs(path):
	directory = dirname(path)
	if not exists(directory):
		os.makedirs(directory)

# Paths

prefix = get_option('prefix', '/usr/local')

libexec_prefix = join(prefix, 'lib')
share_prefix = join(prefix, 'share')

app_libexec_prefix = join(libexec_prefix, 'encode-poppler')
app_modules_prefix = join(share_prefix, 'encode')
app_data_prefix = join(share_prefix, 'encode')

destdir = get_option('destdir', '')

libexec_destdir = destdir + abspath(libexec_prefix)
share_destdir = destdir + abspath(share_prefix)

app_libexec_destdir = destdir + abspath(app_libexec_prefix)
app_modules_destdir = destdir + abspath(app_modules_prefix)
app_data_destdir = destdir + abspath(app_data_prefix)

# Install other files

for path in glob(join('poppler', '*.py')):
	install(join(app_modules_destdir, path), path)

for path in glob(join('poppler', '*.glade')):
	install(join(app_data_destdir, path), path)

poppler_data_destdir = join(app_data_destdir, 'poppler')
make_leading_dirs(poppler_data_destdir)

symlink_path = join(poppler_data_destdir, 'slave')
if os.path.islink(symlink_path) or os.path.exists(symlink_path):
	os.remove(symlink_path)

os.symlink(join(app_libexec_destdir, 'encode-poppler', 'slave'), symlink_path)

# Compile Python modules

if not get_option('no-compile'):
	compileall.compile_dir(join(app_modules_destdir, 'poppler'))
