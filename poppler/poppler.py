# Copyright 2006  Timo Savola
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import os
import struct
import re
import gobject
import gtk
from gettext import gettext

import encode.environment
import encode.view
import encode.execution
import encode.config

COMMAND_CHANNEL_FD = 0
COMMAND_RELOAD = 0x01

datadir = None

def init_extension(d):
	global datadir
	datadir = d

views = {}

def show_file(path, workdir=None):
	if workdir:
		path = os.path.join(workdir, path)

	view = views.get(path)
	if view is not None:
		view.reload()
		encode.environment.show_view(view, focus=False)
	else:
		view = View(path)
		encode.environment.add_view(view, 'top')

class Handler (encode.environment.HandlerExtension):
	def __init__(self):
		self.set_supported('.*\.pdf$')

		self.conf = encode.config.Directory('encode/poppler')
		self.conf.add_string('supported', self.set_supported)

		label = gettext('View with Poppler')
		self.action = gtk.Action(None, label, None, None)
		self.action.set_visible(True)

	def set_supported(self, string):
		self.supported = re.compile(string)

	def get_action(self):
		return self.action

	def supports_file(self, path):
		return self.supported.match(path) is not None

	def open_file(self, path, workdir=None, line=None):
		show_file(path, workdir)

gobject.type_register(Handler)

class View (encode.view.Embedder):
	def __init__(self, path):
		encode.view.Embedder.__init__(self)

		text = gettext('Poppler')
		self.label = gtk.Label(text)
		self.label.show()

		self.path = path
		self.command = os.path.join(datadir, 'poppler', 'slave')
		self.gladepath = os.path.join(datadir, 'poppler', 'slave.glade')

		self.executor = encode.execution.Executor()
		self.channel = self.executor.redirect_input(COMMAND_CHANNEL_FD)

	def client_start(self, xid):
		views[self.path] = self

		uri = 'file://' + self.path
		args = [self.command, str(xid), self.gladepath, str(COMMAND_CHANNEL_FD), uri]
		self.executor.start(encode.execution.Job(args))

	def client_destroy(self):
		if views.get(self.path) is self:
			del views[self.path]

	def reload(self):
		assert self.executor.is_alive()

		self.channel.write(struct.pack('B', COMMAND_RELOAD))
		self.channel.file.flush()

	def get_label(self):
		return self.label

	def cleanup(self):
		self.executor.stop()

gobject.type_register(View)
