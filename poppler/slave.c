/*
 * Copyright 2006  Timo Savola
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <poppler/glib/poppler.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib-object.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

enum command
{
	COMMAND_RELOAD = 0x01
};

static const char * uri = NULL;
static GtkSpinButton * page_button = NULL;
static GtkSpinButton * zoom_button = NULL;
static GtkLabel * title_label = NULL;
static GtkImage * image = NULL;
static GtkToggleButton * stick_button = NULL;
static GtkAdjustment * adjustment = NULL;
static PopplerDocument * document = NULL;
static char * title = NULL;

static void render_page(void)
{
	int page_number;
	double zoom_factor;
	PopplerPage * page;
	double page_width, page_height;
	GdkPixbuf * pixbuf;
	int width, height;

	g_assert(page_button);
	g_assert(zoom_button);
	g_assert(stick_button);
	g_assert(document);
	g_assert(image);

	page_number = gtk_spin_button_get_value_as_int(page_button);
	zoom_factor = gtk_spin_button_get_value(zoom_button) / 100.0;

	page = poppler_document_get_page(document, page_number - 1);
	poppler_page_get_size(page, &page_width, &page_height);

	width = (int) (page_width * zoom_factor);
	height = (int) (page_height * zoom_factor);

	pixbuf = gtk_image_get_pixbuf(image);

	if (pixbuf && (gdk_pixbuf_get_width(pixbuf) != width ||
	               gdk_pixbuf_get_height(pixbuf) != height))
		pixbuf = NULL;

	if (pixbuf)
		g_object_ref(pixbuf);
	else
		pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, width, height);

	poppler_page_render_to_pixbuf(page, 0, 0, width, height, zoom_factor, 0, pixbuf);

	gtk_image_set_from_pixbuf(image, pixbuf);
	g_object_unref(pixbuf);

	if (!gtk_toggle_button_get_active(stick_button))
		gtk_adjustment_set_value(adjustment, 0);
}

static void load_document(void)
{
	GError * error = NULL;
	PopplerDocument * new_document;
	int page_number;
	int page_count;
	char * old_title;
	char * text;

	g_assert(uri);
	g_assert(page_button);
	g_assert(title_label);

	/* Load */

	new_document = poppler_document_new_from_file(uri, NULL, &error);
	if (error) {
		fprintf(stderr, "%s\n", error->message);
		g_error_free(error);
		g_object_unref(new_document);
		return;
	}

	if (document)
		g_object_unref(document);

	document = new_document;

	/* Page numbers */

	page_number = gtk_spin_button_get_value_as_int(page_button);

	page_count = poppler_document_get_n_pages(document);
	gtk_spin_button_set_range(page_button, 1, page_count);

	if (page_number > page_count)
		page_number = page_count;

	gtk_spin_button_set_value(page_button, page_number);

	/* Title */

	g_object_get(document, "title", &text, NULL);
	title = g_strconcat("<b>", text, "</b>", NULL);
	g_free(text);

	old_title = title;

	gtk_label_set_markup(title_label, title);

	if (old_title)
		g_free(old_title);

	/* Render */

	render_page();
}

static inline void create_interface(GdkNativeWindow xid, const char * gladepath)
{
	GtkWidget * plug;
	GladeXML * xml;
	GtkWidget * root;
	GtkWidget * scroll;

	plug = gtk_plug_new(xid);
	g_signal_connect(plug, "destroy", G_CALLBACK (gtk_main_quit), NULL);
	gtk_widget_show(plug);

	xml = glade_xml_new(gladepath, NULL, NULL);
	if (!xml) {
		fprintf(stderr, "Failed to load a Glade interface definition from file \"%s\"\n", gladepath);
		exit(1);
	}

	glade_xml_signal_connect(xml, "on_reload_clicked", G_CALLBACK (load_document));
	glade_xml_signal_connect(xml, "on_page_changed", G_CALLBACK (render_page));
	glade_xml_signal_connect(xml, "on_zoom_changed", G_CALLBACK (render_page));

	root = glade_xml_get_widget(xml, "root");
	gtk_container_add(GTK_CONTAINER (plug), root);

	page_button = GTK_SPIN_BUTTON (glade_xml_get_widget(xml, "page_button"));
	g_object_ref(page_button);

	zoom_button = GTK_SPIN_BUTTON (glade_xml_get_widget(xml, "zoom_button"));
	g_object_ref(zoom_button);

	title_label = GTK_LABEL (glade_xml_get_widget(xml, "title_label"));
	g_object_ref(title_label);

	stick_button = GTK_TOGGLE_BUTTON (glade_xml_get_widget(xml, "stick_button"));
	g_object_ref(stick_button);

	image = GTK_IMAGE (glade_xml_get_widget(xml, "image"));
	g_object_ref(image);

	scroll = glade_xml_get_widget(xml, "scrolled_window");
	adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW (scroll));
	g_object_ref(adjustment);
}

static gboolean handle_command(GIOChannel * channel, GIOCondition condition, gpointer data)
{
	char command = 0;
	GError * error = NULL;

	switch (g_io_channel_read_chars(channel, &command, 1, NULL, &error)) {
	case G_IO_STATUS_EOF:
		return FALSE;

	case G_IO_STATUS_ERROR:
		g_assert(error);

		fprintf(stderr, "%s\n", error->message);
		exit(1);

	case G_IO_STATUS_AGAIN:
		g_assert_not_reached();

	default:
		g_assert(!error);

		switch (command) {
		case COMMAND_RELOAD:
			load_document();
			break;

		default:
			fprintf(stderr, "Unknown command: 0x%02x\n", command);
			break;
		}
	}

	return TRUE;
}

static inline void create_channel(int fd)
{
	GIOChannel * channel;

	channel = g_io_channel_unix_new(fd);
	g_io_channel_set_encoding(channel, NULL, NULL);
	g_io_add_watch(channel, G_IO_IN, handle_command, NULL);
}

int main(int argc, char ** argv)
{
	GdkNativeWindow xid;
	const char * gladepath;
	int fd;

	gtk_init(&argc, &argv);
	glade_init();

	if (argc != 5) {
		fprintf(stderr, "Usage: %s XID GLADEPATH FD URI\n", argv[0]);
		return 1;
	}

	xid = atol(argv[1]);
	gladepath = argv[2];
	fd = atoi(argv[3]);
	uri = argv[4];

	create_interface(xid, gladepath);
	create_channel(fd);

	load_document();

	gtk_main();
	return 0;
}
