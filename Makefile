OPTIMIZE		= no

PREFIX			= /usr/local
LIBEXEC_PREFIX		= $(PREFIX)/lib
APP_LIBEXEC_PREFIX	= $(LIBEXEC_PREFIX)/encode-poppler

DESTDIR			= 
APP_LIBEXEC_DESTDIR	= $(DESTDIR)$(APP_LIBEXEC_PREFIX)

CCFLAGS			= -g -Wall -Wextra -Wno-unused
CPPFLAGS		= -DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED
LDFLAGS			= 

ifeq ($(OPTIMIZE),yes)
CCFLAGS			+= -O2 -fomit-frame-pointer
CPPFLAGS		+= -DNDEBUG -DG_DISABLE_ASSERT
else
CCFLAGS			+= -Werror
endif

build: poppler/slave

poppler/slave: poppler/slave.c
	$(CC) $(CPPFLAGS) $(CCFLAGS) -o $@ $^ $(LDFLAGS) `pkg-config --cflags --libs gtk+-2.0 libglade-2.0 poppler-glib`

install: poppler/slave
	install -d $(APP_LIBEXEC_DESTDIR)
	install $< $(APP_LIBEXEC_DESTDIR)/slave

clean:
	rm -f poppler/slave

.PHONY: build install clean
